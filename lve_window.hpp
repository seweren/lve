#pragma once

#include <vulkan/vulkan.hpp>
#define GLFW_INCLUDE_VULCAN
#include <GLFW/glfw3.h>

namespace lve
{
    class LveWindow
    {
    public:
        LveWindow(int w, int h, std::string name);
        ~LveWindow();

        LveWindow(const LveWindow &) = delete;
        LveWindow &operator=(const LveWindow &) = delete;

        VkExtent2D getExtent() { return {static_cast<uint32_t>(width), static_cast<uint32_t>(height)}; }

        bool shouldClose() { return glfwWindowShouldClose(window); }
        bool wasFrameBufferResized() { return frameBufferResized; }
        void resetFrameBufferResizedFlag() { frameBufferResized = false; }
        void createWindowSurface(VkInstance instance, VkSurfaceKHR *surface);

    private:
        static void frameBufferResizeCallback(GLFWwindow *window, int width, int height);

        void initWindow();
        int width;
        int height;
        bool frameBufferResized = false;
        std::string windowName;
        GLFWwindow *window;
    };

} // namespace lve
