cmake_minimum_required(VERSION 3.16)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# set the project name and version
project(lve VERSION 1.0)

add_compile_definitions(VK_VERSION_1_0)

file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/shaders)

add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/shaders/simple_shader.frag.spv
                   COMMAND glslc ${CMAKE_SOURCE_DIR}/shaders/simple_shader.frag -o ${CMAKE_BINARY_DIR}/shaders/simple_shader.frag.spv
                   DEPENDS ${CMAKE_SOURCE_DIR}/shaders/simple_shader.frag)
add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/shaders/simple_shader.vert.spv
                   COMMAND glslc ${CMAKE_SOURCE_DIR}/shaders/simple_shader.vert -o ${CMAKE_BINARY_DIR}/shaders/simple_shader.vert.spv
                   DEPENDS ${CMAKE_SOURCE_DIR}/shaders/simple_shader.vert)

# add the executable
add_executable(lve 
    main.cpp
    lve_window.cpp
    first_app.cpp
    lve_pipeline.cpp
    lve_device.cpp
    lve_swap_chain.cpp
    lve_model.cpp
    ${CMAKE_BINARY_DIR}/shaders/simple_shader.frag.spv
    ${CMAKE_BINARY_DIR}/shaders/simple_shader.vert.spv
)

target_include_directories(lve PRIVATE .)

target_compile_options(lve PUBLIC
    -O2
)
target_link_libraries(lve PUBLIC
    -lglfw 
    -lvulkan
    -ldl
    -lpthread
    -lX11
    -lXxf86vm
    -lXrandr
    -lXi
)